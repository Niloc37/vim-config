set nocompatible

filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.config/nvim/pluggedd')
"""""""""""""""""""""""""""""""""""""""""""""""
" Library for other plugins
Plug 'eparreno/vim-l9'

" Global vim appearance
Plug 'vim-airline/vim-airline'
Plug 'enricobacis/vim-airline-clock'
Plug 'vim-airline/vim-airline-themes'
Plug 'terryma/vim-smooth-scroll'

" Themes
Plug 'morhetz/gruvbox'
Plug 'Hirezias/gruvpink'
Plug 'Hirezias/gruvayu'

" Dont know if useful
" Plug 'RRethy/vim-illuminate'
" Plug 'Yggdroot/indentLine'
Plug 'djoshea/vim-autoread'
" Plug 'easymotion/vim-easymotion'
" Plug 'kshenoy/vim-signature'

" Navigation
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'

" autocomplete
" Plug 'Valloric/YouCompleteMe'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Valloric/ListToggle'

"auto set indent parameters
" Plug 'tpope/vim-sleuth'

" Git
Plug 'airblade/vim-gitgutter'

" Language support
" Plug 'Raimondi/delimitMate'
Plug 'tomtom/tcomment_vim'
Plug 'jaxbot/semantic-highlight.vim'
Plug 'seerun/vim-polyglot'
Plug 'ziglang/zig.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

Plug 'rhysd/vim-clang-format'

"""""""""""""""""""""""""""""""""""""""""""""""
call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""

set encoding=utf8 nobomb
let mapleader = ','

set termguicolors

set clipboard=unnamedplus

" Gui and style
colorscheme gruvayu
" colorscheme molokai
noremap <silent> ,a :set background=dark<CR>
noremap <silent> ,A :set background=light<CR>

set guifont=DejaVu\ Sans\ Mono:h10
set title
set showcmd
set belloff=all

" Airline config
set laststatus=2 "pour afficher la airline avec un seul fichier
set ruler wildmenu
let g:airline#extensions#clock#format = '%H:%M'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" Show command result
set inccommand=split

" No redraw during macro application
set lazyredraw

" Backup and swap files
set autowrite undofile
set backupdir=~/.config/nvim/tmp/backup                   " for the backup files
set directory=~/.config/nvim/tmp/swap                   " for the swap files
set undodir=~/.config/nvim/tmp/undo                     " for the undo history file

" enable mouse
set mouse=a

" Cursor navigation
set number relativenumber
set cursorline
set colorcolumn=100

" Basic formatting
set linebreak nowrap
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
autocmd Filetype asm setlocal tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab nosmarttab

" Scrolling limits
set scrolloff=8 sidescroll=8

" Search
set hlsearch
nnoremap <silent> <leader>/ :noh<return><esc>
" set smartcase ignorecase

" Invisible chars
set list listchars=tab:►─,nbsp:␣,extends:▷,precedes:◁,trail:░  "◼ ",eol:¶,space:\ ,
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/

" Delete comment character when joining commented lines
if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j
endif

" Split buffers
nnoremap <C-Q> :q<CR>
nnoremap <C-E> :vsplit<CR>
set splitbelow splitright

" Buffer navigation
" nnoremap <silent> <C-J> :bn<CR>
" nnoremap <silent> <C-K> :bp<CR>
nnoremap <silent> <C-H> <C-W>h
nnoremap <silent> <C-J> <C-W>j
nnoremap <silent> <C-K> <C-W>k
nnoremap <silent> <C-L> <C-W>l
nnoremap <silent> <C-Right> :vertical resize +5<CR>
nnoremap <silent> <C-Left> :vertical resize -5<CR>
nnoremap <silent> <C-Up> :resize +5<CR>
nnoremap <silent> <C-Down> :resize -5<CR>
nnoremap <silent> <leader>j :bn<CR>
nnoremap <silent> <leader>k :bp<CR>
nnoremap <silent> <leader>d :bp\|bd #<CR>
nnoremap <silent> <leader>D :bp!\|bd #<CR>

" Toggle wrapping
noremap <silent> <leader>w :set wrap! wrap?<CR>

" Leader C is the prefix for code related mappîngs
noremap <silent> <Leader>cc :TComment<CR>

" Shortcut for Nerdtree
noremap <silent> <leader>n :NERDTreeToggle<CR>

" Shortcut for Vista
" noremap <silent> <leader>t :Vista!!<CR>
" let g:vista_echo_cursor=0
" let g:vista_sizebar_width=50

" Fzf shortcuts
noremap <silent> ; :Buffers<CR>
noremap <silent> <leader>ff :Files<CR>
noremap <silent> <C-p> :GFiles<CR>
noremap <silent> <leader>fl :BLines<CR>
noremap <silent> <leader>L :BLines<CR>
noremap <silent> <leader>fL :Lines<CR>
noremap <silent> <leader>fc :Commands<CR>
noremap <silent> <leader>fm :Marks<CR>
noremap <silent> <leader>C :Colors<CR>
let g:fzf_layout = { 'left': '~70%' }

" Exit edit mode in terminal with Shift-Esc
tnoremap <esc> <C-\><C-n>
tnoremap <C-Q> <esc>
nnoremap <C-T> :terminal<CR>
packadd termdebug
nnoremap <F3> :call TermDebugSendCommand('run')<CR>
nnoremap <F4> :call TermDebugSendCommand('rerun')<CR>
nnoremap <F5> :Continue<CR>
nnoremap <F8> :Finish<CR>
nnoremap <F9> :Over<CR>
nnoremap <F10> :Step<CR>
nnoremap <F11> :Break<CR>
nnoremap <F12> :Clear<CR>

set conceallevel=0

" Build latex to pdf
noremap <leader>bt :w<CR>:! mkdir -p tex_bin; pdflatex -synctex=1 -file-line-error -halt-on-error -output-directory tex_bin -interaction=nonstopmode %<CR>

" Execute current script
nnoremap <leader>e :w\|!./%<CR>

" C++ higlighting
" let g:cpp_class_scope_highlight = 1
" let g:cpp_member_variable_highlight = 1
" let g:cpp_class_decl_highlight = 1
" There are two ways to highlight template functions. Either
" let g:cpp_experimental_simple_template_highlight = 1 " which works in most cases, but can be a little slow on large files. Alternatively set
" let g:cpp_experimental_template_highlight = 1 " which is a faster implementation but has some corner cases where it doesn't work.
" let g:cpp_concepts_highlight = 1
let g:cpp_posix_standard = 1
" let g:cpp_no_function_highlight = 0

let g:zig_fmt_autosave = 1

" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

let g:lt_location_list_toggle_map = '<leader>l'
let g:lt_quickfix_list_toggle_map = '<leader>q'
let g:lt_height = 10

" Semantic highlight configuration
noremap <silent> <leader>s :SemanticHighlightToggle<CR>
let g:semanticTermColors = [154,82,113,222,69,48,184,72,70,143,120,87,133,200,208,123,159,179,221,86,204,177,195,98,227,84,83,43,31,104,49,216,215,146,81,140,201,44,226,42,136,151,71,174,149,197,198,172,41,210,192,138,217,106,62,199,202,115,213,37,63,212,77,196,214,223,119,144,38,220,188,118,206,111,139,173,134,80,153,189,75,224,79,45,193,178,228,76,147,158,141,157,182,186,169,85,114,187,225,109,108,207,51,218,230,137,211,191,142,229,105,209,117,155,135,205,180,185,175,26,148,27,68,190,122,121,176,33,150,116,74,112,46,110,156,194,171,40,73,99,78,203,183,39,97,50,170,152,219,145,47,32,107,181]

" Consider all text files as markdown
autocmd BufRead,BufNewFile *.txt set filetype=markdown wrap
autocmd BufRead,BufNewFile CMakeLists.txt set filetype=cmake
autocmd BufRead,BufNewFile *.md set filetype=markdown wrap
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0

let g:tex_conceal=""
"abdgm"

let g:clang_format#detect_style_file = 1
let g:clang_format#auto_format = 1
let g:clang_format#enable_fallback_style = 0

" hi link illuminatedWord Visual
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 7, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 7, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 7, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 7, 4)<CR>

nnoremap <silent> <leader>c gc

setlocal matchpairs+=<:>
setlocal cinoptions+=j1

inoremap jk <ESC>
inoremap <C-c> <ESC>
inoremap <C-l> ·
inoremap <C-c> ∘

nnoremap <silent> <leader>r :source $MYVIMRC<CR>
