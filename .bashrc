# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
  C_NC='\[\033[0m\]'
  C_NAME='\[\033[1;36m\]'
  C_HOST='\[\033[1;32m\]'
  C_DIR='\[\033[1;33m\]'
  C_BRANCH='\[\033[1;34m\]'
  C_SEP='\[\033[0m\]\[\033[1;37m\]'
  C_ERROR='\[\033[7;31m\]'
  C_PROMPT='\[\033[1;34m\]'
else
  C_NC=
  C_NAME=
  C_HOST=
  C_DIR=
  C_BRANCH=
  C_SEP=
  C_ERROR=
  C_PROMPT=
fi

error_PS1()
{
  local res=$?
  if [[ $res != 0 ]] ; then
    echo $res
  fi
}
git_PS1()
{
  local res=$(git branch 2>/dev/null | grep "^*" | colrm 1 2)
  if [[ $res != "master" ]] ; then
    echo $res
  fi
}

PS1="${C_ERROR}\$(error_PS1)${C_NC}\n${C_NAME}\u${C_SEP}@${C_HOST}\h${C_SEP}:${C_DIR}\w${C_SEP}:${C_BRANCH}\$(git_PS1)${C_NC} ${C_PROMPT}\$${C_NC} "
PS2="${C_PROMPT}>${C_NC} "

unset C_NC C_NAME C_DIR C_BRANCH C_HOST C_SEP C_ERROR C_PROMPT
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias rgrep='rgrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

alias _vmake="(cd build ; make VERBOSE=1 -j2)"
alias _cmake="(mkdir -p build ; cd build ; cmake -G \"Unix Makefiles\" ..)"
alias _jmp="echo -e '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'"
alias t="tree -L 1 -DFtrA --si --noreport"
alias tt="tree -L 2 -DFtrA --si --noreport"
alias ttt="tree -L 3 -DFtrA --si --noreport"
alias tttt="tree -L 4 -DFtrA --si --noreport"


export PATH=$PATH:~/Gits/Scripts

