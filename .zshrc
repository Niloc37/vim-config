# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/alexis/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="../../.config/zsh/perso"
#ZSH_THEME="agnoster"
#ZSH_THEME="maran"
#ZSH_THEME="geoffgarside"
#ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.  DISABLE_UPDATE_PROMPT="true" 
# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  bundler
  dotenv
  rbenv
)

source $ZSH/oh-my-zsh.sh

# User configuration
# prompt="%B%F{cyan}%n%f@%F{green}%M%f:%F{yellow}%~%f $%b "

# Vi mode or emacs mode
bindkey -e

export PATH=$PATH:~/Gits/Scripts:~/Gits/tsp/eta/eta_interceptor/build:~/Gits/tsp/eta/eta_factorization/install/bin

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=fr_FR.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias _vmake="(cd build ; make VERBOSE=1 -j2)"
alias _cmake="(mkdir -p build ; cd build ; cmake -G \"Unix Makefiles\" ..)"
alias _jmp="echo -e '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'"
alias t="tree -L 1 -DFtrA --si --noreport"
alias tt="tree -L 2 -DFtrA --si --noreport"
alias ttt="tree -L 3 -DFtrA --si --noreport"
alias tttt="tree -L 4 -DFtrA --si --noreport"
alias list_installed_packages='gunzip -c /var/log/apt/history.log.*.gz | grep "apt\(-get\)\? install" | cut -d " " -f 4 | sort -u'

# opam configuration
test -r /home/alexis/.opam/opam-init/init.zsh && . /home/alexis/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
alias :q=exit
alias :w=""

# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh/histfile
HISTSIZE=1000
SAVEHIST=10000
setopt appendhistory extendedglob
unsetopt autocd beep notify
bindkey -e
# End of lines configured by zsh-newuser-install

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
export VISUAL=nvim
export EDITOR="$VISUAL"
export PEAGER="bat"
export LC_MESSAGES=en_GB.UTF-8
export LANGUAGE=fr_FR.UTF-8
export LC_ALL=fr_FR.UTF-8
export JULIA_NUM_THREADS=8
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig"

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

alias icat="kitty icat --align=left"
alias isvg="rsvg-convert | icat"
alias idot="dot -Tsvg | icat"

export ETA_INTERCEPT_MODULES=~/Gits/tsp/eta/eta_interceptor/build
